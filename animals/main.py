from tensorflow import keras
import tensorflow as tf

from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.models import Sequential
from keras.layers import Input, Lambda, Dense, Flatten

from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
# from keras.backend.tensorflow_backend import set_session

import scipy
# from PIL.Image import load_img

ANIMALS_PATH = "./img"
IMAGE_SIZE = [224, 224]
ANIMAL_TYPES = 90
BATCH_SIZE = 1000
EPOCHS = 3

# config = tf.configProto()
# config.gpu_options.per_process_gpu_memory_fraction = 0.8
# set_session(tf.Session(config=config))

animal_count = 80
# img_height = 180
# img_width = 1808

model = VGG16(input_shape =IMAGE_SIZE + [3], weights='imagenet', include_top=False)

# freeze layers of predefined model.
for layer in model.layers:
    layer.trainable = False

# add a flatenning layer and output layer.
FlattenedLayer = Flatten()(model.output)
OutputLayer = Dense(ANIMAL_TYPES, activation='softmax')(FlattenedLayer)

model = Model(inputs=model.input, outputs=OutputLayer)

model.compile(
                      loss='categorical_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy']
                    )

model.summary()


ImageGen = ImageDataGenerator(rescale=1./255, validation_split = 0.2)

TrainGen = ImageGen.flow_from_directory(
                                                    directory=ANIMALS_PATH,
                                                    target_size=IMAGE_SIZE,
                                                    batch_size=BATCH_SIZE,
                                                    class_mode='categorical',
                                                    shuffle=False,
                                                    subset='training',
                                                    interpolation='bicubic',
                                        )

TestGen  = ImageGen.flow_from_directory(
                                                    directory=ANIMALS_PATH,
                                                    target_size=IMAGE_SIZE,
                                                    batch_size=BATCH_SIZE,
                                                    class_mode='categorical',
                                                    shuffle=False,
                                                    subset='validation',
                                                    interpolation='bicubic',
                                        )

#AnimalLabels = TrainGen.class_indices

stats = model.fit(
    TrainGen,
    steps_per_epoch=TrainGen.samples,
    epochs=EPOCHS,
    validation_data=TestGen,
    validation_steps=TestGen.samples
)

model.save('animals.h5')