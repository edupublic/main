from edu import *
import tensorflow as tf
from keras.datasets import mnist
import numpy as np
from keras.utils import to_categorical
import os


class DData:
    data = Data()

    @staticmethod
    def diag_item(matrix_2d):
        shape = (11, 28)
        s_shape = (1, 28)
        mtx = np.zeros(shape=shape, dtype=np.float)
        r1 = np.zeros(shape=s_shape, dtype=np.float)
        r2 = np.zeros(shape=s_shape, dtype=np.float)
        r3 = np.zeros(shape=s_shape, dtype=np.float)
        r4 = np.zeros(shape=s_shape, dtype=np.float)
        r5 = np.zeros(shape=s_shape, dtype=np.float)
        r6 = np.zeros(shape=s_shape, dtype=np.float)
        r7 = np.zeros(shape=s_shape, dtype=np.float)
        r8 = np.zeros(shape=s_shape, dtype=np.float)
        r9 = np.zeros(shape=s_shape, dtype=np.float)
        r10 = np.zeros(shape=s_shape, dtype=np.float)
        r11 = np.zeros(shape=s_shape, dtype=np.float)

        r1[0, :28 - 5] = np.diag(matrix_2d, -5)
        r2[0, :28 - 4] = np.diag(matrix_2d, -4)
        r3[0, :28 - 3] = np.diag(matrix_2d, -3)
        r4[0, :28 - 2] = np.diag(matrix_2d, -2)
        r5[0, :27] = np.diag(matrix_2d, -1)
        r6 = np.diag(matrix_2d, 0)
        r7[0, :28-1] = np.diag(matrix_2d, 1)
        r8[0, :28-2] = np.diag(matrix_2d, 2)
        r9[0, :28-3] = np.diag(matrix_2d, 3)
        r10[0, :28-4] = np.diag(matrix_2d, 4)
        r11[0, :28-5] = np.diag(matrix_2d, 5)

        np.append(r5, np.zeros(4))

        mtx[0] = r1
        mtx[1] = r2
        mtx[2] = r3
        mtx[3] = r4
        mtx[4] = r5
        mtx[5] = r6
        mtx[6] = r7
        mtx[7] = r8
        mtx[8] = r8
        mtx[9] = r10
        mtx[10] = r11

        return mtx

    def extract_diag(self, matrix_3d):
        first = matrix_3d[0]
        first_item = self.diag_item(first)
        print(first_item.shape)
        shape = (matrix_3d.shape[0], first_item.shape[0], first_item.shape[1])
        result = np.zeros(shape)
        result[0] = first_item
        for index in range(1, shape[0] - 1):
            current_item = matrix_3d[index]
            result[index] = self.diag_item(current_item)

        return result

    def first_data(self):
        (x_train, y_train), (x_test, y_test) = self.data.mnist()
        x_train = self.extract_diag(x_train)
        x_test = self.extract_diag(x_test)
        y_train = to_categorical(y_train)
        y_test = to_categorical(y_test)

        return x_train, y_train, x_test, y_test

    def second_data(self):
        (x_train, y_train), (x_test, y_test) = self.data.mnist()
        x_train = self.extract_diag(np.transpose(x_train, (0, 2, 1)))
        x_test = self.extract_diag(np.transpose(x_test, (0, 2, 1)))
        y_train = to_categorical(y_train)
        y_test = to_categorical(y_test)

        return x_train, y_train, x_test, y_test


class DNets:
    @staticmethod
    def model(x_train, y_train, x_test, y_test, index):
        h = x_train.shape[1]
        w = x_train.shape[2]
        model = tf.keras.models.Sequential([
            tf.keras.layers.Conv2D(h*w, (5, 5), activation=tf.keras.activations.relu, input_shape=(h, w, 1)),
            tf.keras.layers.Flatten(input_shape=(x_train.shape[1], x_train.shape[2])),
            tf.keras.layers.Dense(x_train.shape[1] * x_train.shape[2] * 3, activation=tf.keras.activations.relu),
            tf.keras.layers.Dense(10)
        ])

        # если модель первой версии -- оставить пустым
        version = "v1"

        if os.path.isdir('./models/' + str(index) + '_d_model' + version):
            model = tf.keras.models.load_model('./models/' + str(index) + '_model' + version)
        else:
            model.compile(
                optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy']
            )

            model.fit(
                x_train,
                y_train,
                batch_size=1,  # 128,
                epochs=100
            )

            model.evaluate(x_test, y_test)

            model.save('./models/' + str(index) + '_model')

        return model
