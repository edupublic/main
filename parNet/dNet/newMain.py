from dEdu import *

def main():
    data = DData()
    nets = DNets()

    # для прямого хода
    x_train_st, y_train_sr, x_test_sr, y_test_sr = data.first_data()
    # для обратного хода
    x_train_rc, y_train_rc, x_test_rc, y_test_rc = data.second_data()
    sr_model = nets.model(
        x_train_st,
        y_train_sr,
        x_test_sr,
        y_test_sr,
        1)

    rc_model = nets.model(
        x_test_rc,
        y_test_rc,
        x_test_rc,
        y_test_rc,
        2)



if __name__ == '__main__':
    main()
