import numpy as np


class Sep:
    @staticmethod
    def sep_image_v_full(im: np.ndarray) -> np.ndarray:
        im_c = im.copy()
        im_c[:, 0:10] = 0
        im_c[:, 20:28] = 0
        return im_c

    @staticmethod
    def not_z_points_ind(im: np.ndarray) -> np.ndarray:
        # попробовать оценить только индексы чисел
        res = []
        for i in range(28):
            for j in range(28):
                if im[i, j] in range(200, 255):
                    res.append([i, j])
        return np.array(res)
