import tensorflow as tf
from keras.datasets import mnist
import numpy as np
from keras.utils import to_categorical
import os


class Data:
    num_train = 60000
    num_test = 10000
    h = 28
    w = 28
    depth = 1
    num_classes = 10

    def mnist(self):
        return mnist.load_data()

    def netData(self, multipyer):
        (x_train, y_train), (x_test, y_test) = self.mnist()
        lower = 7 * (multipyer - 1)
        upper = 7 * multipyer
        x_train = x_train[:, :, lower:upper]
        # y_train = y_train
        x_test = x_test[:, :, lower:upper]
        # y_test = y_test[:, :, lower:upper]

        return x_train, y_train, x_test, y_test

    def diagNetData(self):
        (x_train, y_train), (x_test, y_test) = self.mnist()
        first = x_train[0]
        firstItem = np.array([
            np.diag(first, -2),
            np.diag(first, -1),
            np.diag(first, 0),
            np.diag(first, 1),
            np.diag(first, 2),
        ])
        x_train_new = np.zeros((x_train.shape[0], firstItem.shape[0], firstItem.shape[1]))
        x_train_new[0] = firstItem

        first_item_x_train = self.first_data_for_diag(x_train)
        x_train_new = self.iteration_for_diag(x_train, first_item_x_train)

    def first_data_for_diag(self, matrix):
        first = matrix[0]
        firstItem = np.array([
            np.diag(first, -2),
            np.diag(first, -1),
            np.diag(first, 0),
            np.diag(first, 1),
            np.diag(first, 2),
        ])
        return firstItem

    def iteration_for_diag(self, matrix, first_item):
        res = np.zeros(matrix.shape[0], first_item.shape[0], first_item.shape[1])
        res[0] = first_item
        for index in range(1, matrix.shape[0] - 1):
            item = matrix[index]
            current_item = np.array([
                np.diag(item, -2),
                np.diag(item, -1),
                np.diag(item, 0),
                np.diag(item, 1),
                np.diag(item, 2),
            ])
            res[index] = current_item
        return res

    def prepare(self, x_train, y_train, x_test, y_test):
        # x_train = x_train.reshape(self.num_train, self.h*self.w)
        # x_test = x_test.reshape(self.num_train, self.h*self.w)
        x_train = x_train.astype(np.float)
        x_test = x_test.astype(np.float)

        y_train = to_categorical(y_train, self.num_classes)
        y_test = to_categorical(y_test, self.num_classes)
        return x_train, y_train, x_test, y_test


class Nets:
    def model(self, x_train, y_train, x_test, y_test, index):
        model = tf.keras.models.Sequential([
            tf.keras.layers.Flatten(input_shape=(28, 7)),
            tf.keras.layers.Dense(132, activation=tf.keras.activations.relu),
            tf.keras.layers.Dense(10)
        ])

        # если модель первой версии -- оставить пустым
        version = "v2"

        if os.path.isdir('./models/' + str(index) + '_model' + version):
            model = tf.keras.models.load_model('./models/' + str(index) + '_model')
        else:
            model.compile(
                optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy']
            )

            model.fit(
                x_train,
                y_train,
                batch_size=32,  # 128,
                epochs=10
            )

            model.evaluate(x_test, y_test)

            model.save('./models/' + str(index) + '_model')

        return model

    def testQuadraNetModel(self, x_train, y_train, x_test, y_test):
        model = tf.keras.models.Sequential([
            tf.keras.layers.Input(4),
            tf.keras.layers.Dense(32, activation=tf.keras.activations.relu),
            tf.keras.layers.Dense(10)
        ])

        if os.path.isdir('./models/qNet'):
            model = tf.keras.models.load_model('./models/qNet')
        else:
            model.compile(
                optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy']
            )

            model.fit(
                x_train,
                y_train,
                batch_size=32,
                epochs=10
            )

            model.evaluate(x_test, y_test)
            model.save('./models/qNet')

        return model
