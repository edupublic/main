import numpy as np
from edu import *
from saver import *
import tensorflow as tf


def main(is_need_fuck=False):
    tf.config.threading.set_inter_op_parallelism_threads(44)
    tf.config.threading.set_intra_op_parallelism_threads(44)

    data = Data()
    x_train1, y_train1, x_test1, y_test1 = data.netData(multipyer=1)
    x_train1, y_train1, x_test1, y_test1 = data.prepare(x_train1, y_train1, x_test1, y_test1)

    x_train2, y_train2, x_test2, y_test2 = data.netData(multipyer=2)
    x_train2, y_train2, x_test2, y_test2 = data.prepare(x_train2, y_train2, x_test2, y_test2)

    x_train3, y_train3, x_test3, y_test3 = data.netData(multipyer=3)
    x_train3, y_train3, x_test3, y_test3 = data.prepare(x_train3, y_train3, x_test3, y_test3)

    x_train4, y_train4, x_test4, y_test4 = data.netData(multipyer=4)
    x_train4, y_train4, x_test4, y_test4 = data.prepare(x_train4, y_train4, x_test4, y_test4)

    net = Nets()
    m1 = net.model(x_train1, y_train1, x_test1, y_test1, 1)
    m2 = net.model(x_train2, y_train2, x_test2, y_test2, 2)
    m3 = net.model(x_train3, y_train3, x_test3, y_test3, 3)
    m4 = net.model(x_train4, y_train4, x_test4, y_test4, 4)

    if is_need_fuck:
        # проверка ебучая
        return

    shape = (1, 28, 7)
    q_shape = (x_test1.shape[0], 1, 10)
    q_shape1 = (x_train1.shape[0], 1, 10)

    q_x1_test = np.zeros(q_shape)
    q_x2_test = np.zeros(q_shape)
    q_x3_test = np.zeros(q_shape)
    q_x4_test = np.zeros(q_shape)

    q_x1_tr = np.zeros(q_shape1)
    q_x2_tr = np.zeros(q_shape1)
    q_x3_tr = np.zeros(q_shape1)
    q_x4_tr = np.zeros(q_shape1)

    test_data_file = './qData/'

    # for index in range(0, x_test1.shape[0] - 1):
    #     percent = round((index / (x_test1.shape[0]-1)) * 100, 3)
    #     print("Итерация тестовых " + str(index) + " из " + str(x_test1.shape[0] - 1) + ". " + str(percent) + '%')
    #     q_x1_test[index] = m1.predict(x_test1[index].reshape(shape), verbose=0)
    #     q_x2_test[index] = m2.predict(x_test2[index].reshape(shape), verbose=0)
    #     q_x3_test[index] = m3.predict(x_test3[index].reshape(shape), verbose=0)
    #     q_x4_test[index] = m4.predict(x_test4[index].reshape(shape), verbose=0)
    #
    # save(test_data_file + '1.npy', q_x1_test)
    # save(test_data_file + '2.npy', q_x2_test)
    # save(test_data_file + '3.npy', q_x3_test)
    # save(test_data_file + '4.npy', q_x4_test)

    for index in range(0, x_train1.shape[0] - 1):
        percent = round((index / (x_train1.shape[0] - 1)) * 100, 3)
        print("Итерация тренировочных " + str(index) + " из " + str(x_train1.shape[0] - 1) + ". " + str(percent) + '%')
        q_x1_tr[index] = m1.predict(x_train1[index].reshape(shape), verbose=0)
        q_x2_tr[index] = m2.predict(x_train2[index].reshape(shape), verbose=0)
        q_x3_tr[index] = m3.predict(x_train3[index].reshape(shape), verbose=0)
        q_x4_tr[index] = m4.predict(x_train4[index].reshape(shape), verbose=0)

    save(test_data_file + 'tr1.npy', q_x1_tr)
    save(test_data_file + 'tr2.npy', q_x2_tr)
    save(test_data_file + 'tr3.npy', q_x3_tr)
    save(test_data_file + 'tr4.npy', q_x4_tr)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main(True)
