import numpy as np

def save(file, matrix):
    with open(file, 'wb') as f:
        np.save(f, matrix)
        f.close()

def load(file):
    with open(file, 'rb') as f:
        return np.load(f)